<?php

namespace App\Http\Controllers;

use App\BilanSante;
use App\Enfants;
use App\ParentsEnfants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EnfantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       
    }
    public function All()
    {
        $enfants = Enfants::all();
        return $enfants->toJson(JSON_PRETTY_PRINT);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {
        $fist3letter = substr($request->nom,0,3);
        $number= random_int(1000,9999);
        $codeunique = $fist3letter.strval($number);
        $enfant = new Enfants([
            'nom'=>$request->nom,
            'prenom'=>$request->prenom,
            'dateDeNaissance'=>$request->dateDeNaissance,
            'codeUnique'=>$codeunique,
            
        ]);
        $enfant->save();
     

        $parentEnfant =new ParentsEnfants([
             'id_enfant'=>$enfant->id,
             'id_parent'=>$id

        ]);
        $parentEnfant->save();
       return response()->json([
            'message' => 'Successfully created ParentEnfant'
        ], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enfants  $enfants
     * @return \Illuminate\Http\Response
     */
    public function show(Enfants $enfants)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enfants  $enfants
     * @return \Illuminate\Http\Response
     */
    public function edit(Enfants $enfants)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enfants  $enfants
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enfants $enfants)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enfants  $enfants
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $enfant=Enfants::findOrFail($id);
        $enfant->delete();
        DB::table('parents_enfants')->where('id_enfant', $id)->delete();
        return response()->json([
            'message' => 'Successfully deleted child'
        ], 201);
    }
    /**
     * search enfants by groupe
     * @param  [int] id_groupe
     * @return [Json] enfants
     */

    public function  EnfantsByGroupe($idGroupe)
    {
        $enfants = DB::table('enfants')->select('*')->where('enfants.idGroupe','=',$idGroupe)->get();
        return $enfants->toJson(JSON_PRETTY_PRINT);
    }
    public function EnfantsParParent($idParent)
    {
        $enfants= DB::table('parents_enfants')
        ->join('enfants', 'Parents_enfants.id_enfant', '=', 'enfants.id')
        ->join('Parents', 'Parents_enfants.id_parent', '=', 'parents.id')->select('enfants.*')->where('parents.id','=',$idParent)->get() ;
      
        return $enfants->toJson(JSON_PRETTY_PRINT);
    }
    public function obtenirDossier($id_bilan){

        $bilan = DB::table('enfants')
        ->join('bilan_santes','enfants.idBilanSante',"=",'bilan_santes.id')
        ->select('bilan_santes.*')->where('bilan_santes.id','=',$id_bilan)->get();
        return $bilan->toJson(JSON_PRETTY_PRINT);
    }
    public function ajouterDossier($id_enfants,Request $request)
    {
       
        $fistLetter = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyz"), 0, 7);
        $bilan = new BilanSante([
            'id'=>$fistLetter,
            'description'=>$request->description,
            'medicament'=>$request->medicament,
            'allergie'=>$request->allergie,

        ]);
        $bilan->save();
        $enfants = Enfants::where('id','=',$id_enfants)->update( array('idBilanSante'=>$fistLetter));
        return response()->json([
            'message' => 'Successfully create bilan'
        ], 201);
    }
    public function updatePresence(Request $request,$id_enfant) 
    {
      $enfant = Enfants::where('enfants.id','=',$id_enfant)->update(array('presence'=>$request->presence));
      return response()->json([
        'message' => 'Update'
    ], 201);
    }
    public function updateBilan($id_bilan, Request $request){
        DB::table('bilan_santes')
 ->where('id', $id_bilan)
 ->update(['medicament' => $request->medicament,
            'description'=>$request->description,
            'allergie'=>$request->allergie
 ]);
        return response()->json([
            'message' => 'Update'
        ], 201);
    }
}
