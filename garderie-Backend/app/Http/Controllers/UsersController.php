<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateIdRole(Request $request, $id)
    {
        $user = User::where('id','=',$id)->update( array('id_role'=>$request->id_role));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Find the role of Specific User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

public function UserRole(Request $request)
{
$idRole= DB::table('users')->select('id_role')->where('users.id','=',$request->id)->first()->id_role;
$role = DB::table('roles')->select('nom')->where('roles.id','=',$idRole)->get();
return $role->toJson(JSON_PRETTY_PRINT);
}

}
