<?php

namespace App\Http\Controllers;

use App\Educatrice;
use App\Educatrices;
use App\Enfants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EducatricesController extends Controller
{


    public function create(Request $request)
    {
      $educatrice = new Educatrices([

        "nom"=>$request->nom,
        "prenom"=>$request->prenom,
        "id_garderie"=>$request->id_garderie,
        "id_groupe"=>$request->id_groupe,
        "id_user"=>$request->id_user
        
      ]);
      $educatrice->save();
      return response()->json([
        'message' => 'Successfully created Educatrice!'
    ], 201);


    }

    public function all(){

    }
  public  function currentEducatrice($id_user){

      $educatrice =DB::table('educatrices')->select('*')->where('educatrices.id_user','=',$id_user)->get();  
    
      return $educatrice->toJson(JSON_PRETTY_PRINT);

  }

    
}
