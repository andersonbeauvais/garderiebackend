<?php

namespace App\Http\Controllers;

use App\CompteRendu;
use Illuminate\Http\Request;

class CompteRenduController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
public function create(Request $request, $id, $id_enfant){

    $compte = new CompteRendu([
        'id_educatrice'=>$id,
        'id_enfants'=>$id_enfant,
        'descripton'=>$request->description

    ]);

    $compte->save();

    return response()->json([
        'message' => 'Successfully created  compte rendu'
    ], 201);
}
}
