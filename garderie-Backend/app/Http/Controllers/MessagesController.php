<?php

namespace App\Http\Controllers;

use App\Messages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * create message for a receiver.
     * @param string $message
     * @param  int  $id_sender
     * @param int $id_receiver
     * @return \Illuminate\Http\Response
     */
    public function senderToreceiver(Request $request)
    {
         $message = new Messages([
            'message'=> $request->message,
            'id_sender'=> $request->id_sender,
            'id_receiver'=>$request->id_receiver
         ]);   

         $message->save();
         return response()->json([
             'message' => 'Successfully created Message!'
         ], 201);
    }
    /**
     *@param int $id
     *@return \Illuminate\Support\Traits\EnumeratesValues::toJson 
    */
    public function receiveToSender(Request $request)
    {
        $message = DB::table('messages')->select('*')->where('messages.id_receiver','=',$request->id)->get();

        return $message->toJson(JSON_PRETTY_PRINT);
    }

}
