<?php

namespace App\Http\Controllers;

use App\Parents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ParentsController extends Controller
{

/**
     * Create parent
     *
     * @param  [string] prenom
     * @param  [string] nom
     * @param  [string] adresse
     * @param  [string] telephone
     * @return [string] message
     */
    function create(Request $request){

        $parent = new Parents([
            "prenom" => $request->prenom,
            "nom"=>$request->nom,
            "adresse"=>$request->adresse,
            "telephone"=>$request->telephone,
            "idGarderie"=> $request->idGarderie,
            "id_user"=>$request->id_user

        ]);

        $parent->save();
        return response()->json([
            'message' => 'Successfully created Parent!'
        ], 201);


    }
    /**
     * Search Parent
     *
     * @param  [string] id_role
    
     * @return [Json] current parent
     */
    function currentParent($id_user){

        $parent =DB::table('parents')->select('*')->where('parents.id_user','=',$id_user)->get();  
      
        return $parent->toJson(JSON_PRETTY_PRINT);

    }
}
