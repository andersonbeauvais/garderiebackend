<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BilanSante extends Model
{
    protected $fillable = [
        "id","allergie","description","medicament",
    ];
}
