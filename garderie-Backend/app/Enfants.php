<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enfants extends Model
{
    //
    protected $fillable = [
        'prenom', "nom","codeUnique","dateDeNaissance","idBilanSante","idGroupe","present","imageUrl"

    ];
}
