<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parents extends Model
{
    //
    protected $fillable = [
        'prenom', "nom","adresse","telephone","idGarderie","id_user"

    ];


    public function user()
{
    return $this->belongsTo('App\User');

}
}
