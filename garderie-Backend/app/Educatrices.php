<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Educatrices extends Model
{
    //
    protected $fillable = [
        "nom","prenom","id_garderie","id_groupe","id_user"
    ];
}
