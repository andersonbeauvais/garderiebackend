<?php
namespace Entity;
use App\BilanSante;
use Illuminate\Database\Seeder;

class BilanSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $bilan = new BilanSante();
        $bilan->id='abc142';
        $bilan->description="Cette enfant a un trouble de l'attention quand il ne mange pas ses carotes il devient violent";
        $bilan->medicament='Vitamin c';
        $bilan->save();

        
        $bilan = new BilanSante();
        $bilan->id='abb0982';
        $bilan->description="Cette enfant est très retarder dans son langage il a des trouble pipolaire ";
        $bilan->medicament='antidépresseurs';
        $bilan->save();

        $bilan = new BilanSante();
        $bilan->id='a3b0982';
        $bilan->description="Cette enfant a été traumatisé par justin";
        $bilan->medicament='antidépresseurs';
        $bilan->save();

        $bilan = new BilanSante();
        $bilan->id='aeb0982';
        $bilan->description="Cette enfant à besoin de beaucoup de lait car il aime sa ";
        $bilan->medicament ='Calcium 500mg comprimé';
        $bilan->allergie= 'beurre de chocolat ';
        $bilan->save();

        $bilan = new BilanSante();
        $bilan->id='abb1082';
        $bilan->description="Cette enfant à besoin de suveillance car il fait des crise de nerf";
        $bilan->medicament ='Ritalin';
        $bilan->save();

        $bilan = new BilanSante();
        $bilan->id='abd3982';
        $bilan->description="Cette enfant à besoin de dormir beaucoup";
        $bilan->medicament ='somnifère';
        $bilan->allergie= 'huile olive';
        $bilan->save();

    }
}
