<?php
namespace Entity;
use App\Messages;
use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $message = new Messages();
        $message->message = "Mon enfant a t-il manger son repas";
        $message->id_sender = 1;
        $message->id_receiver = 2;
        $message->save();

        $message = new Messages();
        $message->message = "Oui il a manger son repas a 12hre";
        $message->id_sender = 2;
        $message->id_receiver = 1;
        $message->save();

        $message = new Messages();
        $message->message = "Arreter de penser que votre enfants n'es pas nourrie par la garderie";
        $message->id_sender = 2;
        $message->id_receiver = 1;
        $message->save();
        
        $message = new Messages();
        $message->message = "Mon enfants n'aime pas le porc arreter de lui en donner";
        $message->id_sender = 1;
        $message->id_receiver = 2;
        $message->save();



    }
}
