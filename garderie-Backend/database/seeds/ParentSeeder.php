<?php

namespace Entity;

use App\Parents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ParentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        //parent 1
        $parent = new Parents();
        $parent->prenom = "jean";
        $parent->nom = "Denis";

        $parent->adresse = '2043 rue Tin-tin';
        $parent->telephone = '514-887-1124';

        $parent->idGarderie = 1;
        $parent->id_user = 1;
        $parent->save();

        //parent 2
        $parent = new Parents();
        $parent->prenom = "Max";
        $parent->nom = "Mathieu";

        $parent->adresse = '2350 rue jean-maude';
        $parent->telephone = "514-881-4474";
        $parent->idGarderie = 1;
        $parent->id_user = 2;
        $parent->save();

        //parent 3
        $parent = new Parents();
        $parent->prenom = "Maxime";
        $parent->nom = "Treamblay";

        $parent->adresse = '2343 rue jean-maude';
        $parent->telephone = "514-881-4474";
        $parent->idGarderie = 1;
        $parent->id_user = 5;
        $parent->save();
    }
}
