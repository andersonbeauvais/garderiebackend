<?php
namespace Entity;

use App\Enfants;
use Illuminate\Database\Seeder;

class EnfantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //enfant Jean
        $enfant = new Enfants();
        $enfant->nom = "Denis";
        $enfant->prenom = "bastin";
        $enfant->codeUnique = "JEABAS145585";
        $enfant->dateDeNaissance="2018-02-02";
        $enfant->idGroupe = 1;
        $enfant->imageUrl = "https://i.ibb.co/NjgC7RZ/denis.png";
        $enfant->idBilanSante = '1';
        $enfant->presence = true;
        $enfant->save();

        // enfant Emilie
        $enfant = new Enfants();
        $enfant->nom = "Denis";
        $enfant->prenom = "Maude";
        $enfant->codeUnique = "EmilMaude145585";
        $enfant->dateDeNaissance="2018-02-02";
        $enfant->idGroupe = 1;
        $enfant->imageUrl = "https://i.ibb.co/kMMqwM5/chinessgirl.png";
        $enfant->idBilanSante = '2';
        $enfant->presence = true;
        $enfant->save();

        // enfant Bastien
        $enfant = new Enfants();
        $enfant->nom = "Mathieu";
        $enfant->prenom = "Roger";
        $enfant->codeUnique = "EmilMaude145585";
        $enfant->dateDeNaissance="2018-02-02";
        $enfant->idGroupe = 1;
        $enfant->imageUrl = "https://i.ibb.co/pWkCN9b/jay.png";
        $enfant->idBilanSante = '3';
        $enfant->presence = true;
        $enfant->save();

        // enfant Mathieu
        $enfant = new Enfants();
        $enfant->nom = "Mathieu";
        $enfant->prenom = "James";
        $enfant->codeUnique = "EmilMaude145585";
        $enfant->dateDeNaissance="2018-02-02";
        $enfant->idGroupe = 1;
        $enfant->imageUrl = "https://i.ibb.co/RgxNr6m/mathieu.png";
        $enfant->idBilanSante = '4';
        $enfant->presence = true;
        $enfant->save();

         //enfant Mathieu jumeau
         $enfant = new Enfants();
         $enfant->nom = "Treamblay";
         $enfant->prenom = "Jimmy";
         $enfant->codeUnique = "EmilMaude145585";
         $enfant->dateDeNaissance="2018-02-02";
         $enfant->idGroupe = 1;
         $enfant->imageUrl = "https://i.ibb.co/RgxNr6m/mathieu.png";
         $enfant->idBilanSante = '5';
         $enfant->presence = true;
         $enfant->save();
     
    }
}
