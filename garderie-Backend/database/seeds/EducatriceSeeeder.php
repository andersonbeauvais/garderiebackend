<?php
namespace Entity;

use App\Educatrices;
use Illuminate\Database\Seeder;

class EducatriceSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       /*  $table->id()->start_from(14010);
        $table->string("nom");
        $table->string("prenom");
        $table->integer("id_garderie");
        $table->integer("id_groupe");
        $table->integer("id_user");
        $table->timestamps(); */

        $educatrice = new Educatrices();
        $educatrice->nom='elaine';
        $educatrice->prenom='michel';
        $educatrice->id_garderie = 1;
        $educatrice->id_groupe=1;
        $educatrice->id_user=3;
        $educatrice->save();

        $educatrice = new Educatrices();
        $educatrice->nom='Diane';
        $educatrice->prenom='michel';
        $educatrice->id_garderie = 1;
        $educatrice->id_groupe=1;
        $educatrice->id_user=4;
        $educatrice->save();
    }
}
