<?php
namespace Entity;
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //user1
        $user = new User();
        $user->email ='parent@hotmail.com';
        $user->password='parent123';
        $user->id_role='1';
        $user->save();
       //user2 
        $user = new User();
        $user->email ='parent2@hotmail.com';
        $user->password='parent123';
        $user->id_role='2';
        $user->save();

        $user = new User();
        $user->email ='parent3@hotmail.com';
        $user->password='parent123';
        $user->id_role='5';
        $user->save();

        $user = new User();
        $user->email ='educatrice@hotmail.com';
        $user->password='educatrice123';
        $user->id_role='3';
        $user->save();

        $user = new User();
        $user->email ='educatrice2@hotmail.com';
        $user->password='educatrice123';
        $user->id_role='4';
        $user->save();

        $user = new User();
        $user->email ='educatrice3@hotmail.com';
        $user->password='educatrice123';
        $user->id_role='6';
        $user->save();

        

    }
}
