<?php
namespace Entity;

use App\ParentsEnfants;
use Illuminate\Database\Seeder;

class ParentEnfantSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $parents = new ParentsEnfants();
        
        $parents->id_parent = 1;
        $parents->id_enfant= 1;
        $parents->save();
        
        $parents = new ParentsEnfants();
        $parents->id_parent = 1;
        $parents->id_enfant= 2;
        $parents->save();
       
        $parents = new ParentsEnfants();
        $parents->id_parent = 2;
        $parents->id_enfant= 3;
        $parents->save();
       
        $parents = new ParentsEnfants();
        $parents->id_parent = 2;
        $parents->id_enfant= 4;
        $parents->save();
        
        $parents = new ParentsEnfants();
        $parents->id_parent = 3;
        $parents->id_enfant= 5;
        $parents->save();
    
    }
}
