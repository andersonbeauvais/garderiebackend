<?php


use Entity\RoleSeeder;
use Entity\UserSeeder;
use Entity\BilanSeeeder;
use Entity\GroupeSeeder;
use Entity\ParentSeeder;
use Entity\EnfantsSeeder;
use Entity\MessageSeeder;
use Entity\GarderieSeeder;
use Entity\EducatriceSeeeder;
use Entity\ParentEnfantSeeeder;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(EnfantsSeeder::class);
        $this->call(GarderieSeeder::class);
        $this->call(GroupeSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(MessageSeeder::class);
        $this->call(ParentSeeder::class);
        $this->call(EducatriceSeeeder::class);
        $this->call(ParentEnfantSeeeder::class);
        $this->call(BilanSeeeder::class);
    }
}
