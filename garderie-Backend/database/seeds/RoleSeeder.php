<?php
namespace Entity;

use App\Roles;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $role = new Roles();
        $role->nom ='Admin';
        $role->save();
        $role = new Roles();
        $role->nom ='Parent';
        $role->save();
        $role = new Roles();
        $role->nom ='Educateur';
        $role->save();
        

    }
}
