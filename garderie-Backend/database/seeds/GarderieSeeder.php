<?php
namespace Entity;

use App\Garderie;
use Illuminate\Database\Seeder;

class GarderieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $garderie= new Garderie();
        $garderie->nom = 'Garderie soleil';
        $garderie->adresse= '4582 rue marquerite';
        $garderie->telephone ='514-466-3432' ;
        $garderie->save();

        $garderie= new Garderie();
        $garderie->nom = 'La terreur de la nuit';
        $garderie->adresse= '1158 rue benoit';
        $garderie->telephone ='514-766-1132' ;
        $garderie->save();
        
        $garderie= new Garderie();
        $garderie->nom = 'Pervers en herbe';
        $garderie->adresse= '1158 rue st-caterine';
        $garderie->telephone ='514-766-4722' ;
        $garderie->save();
        

    }
}
