<?php
namespace Entity;
use App\Groupe;
use Illuminate\Database\Seeder;

class GroupeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $groupe = new Groupe();
        $groupe->numero ='A34';
        $groupe->idGarderie=1;
        $groupe->save();

        $groupe = new Groupe();
        $groupe->numero ='B36';
        $groupe->idGarderie=1;
        $groupe->save();

        $groupe = new Groupe();
        $groupe->numero ='B34';
        $groupe->idGarderie=2;
        $groupe->save();


        $groupe = new Groupe();
        $groupe->numero ='C24';
        $groupe->idGarderie=2;
        $groupe->save();


        $groupe = new Groupe();
        $groupe->numero ='U14';
        $groupe->idGarderie=3;
        $groupe->save();
    }
}
