<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompteRendusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compte-rendus', function (Blueprint $table) {
            $table->id();
            $table->integer('id_enfant');
            $table->integer('id_educatrice');
            $table->text('descrition');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return 
     */
    public function down()
    {
        Schema::dropIfExists('compte-rendus');
    }
}
