<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::apiResources(['
        Enfants'=>'EnfantsController',
        'Parents'=>'ParentsController',
        'Garderie'=>'GarderieController',
        'Groupe'=>'GroupeController',
        'Messages'=>'MessagesController',
        'User'=>'UsersController',
        'CompteRendu'=>'CompteRenduController'
        ]);
Route::get('user/role',"UsersController@UserRole");
Route::post('message/sender','MessagesController@senderToreceiver');
Route::get('messsage/receiver','MessagesController@receiveToSender');
Route::post('parent/create','ParentsController@create');
Route::post('educatrice/create','EducatricesController@create');
Route::get('garderie/list','GarderieController@all');
Route::get('garderie/GroupeParGarderie','GroupeController@groupeBygarderie');
Route::post('ajouter/enfants/{id}','EnfantsController@create');
Route::get('currentParent/{id_user}','ParentsController@currentParent');
Route::get('currentEducatrice/{id_user}','EducatricesController@currentEducatrice');
Route::get('enfantsByGroupe/{idGroupe}','EnfantsController@EnfantsByGroupe');
Route::put('updateIdRole/{id}','UsersController@updateIdRole');
Route::get('enfantsByParent/{idParent}','EnfantsController@EnfantsParParent');
Route::get('enfant/obtenirBilan/{id_bilan}','EnfantsController@obtenirDossier');
Route::post('enfant/bilan/create/{id}','EnfantsController@ajouterDossier');
Route::put('updatePresence/{id_enfant}','EnfantsController@updatePresence');
Route::put('updateBilan/{id_bilan}','EnfantsController@updateBilan');
Route::delete('deleteEnfant/{id}','EnfantsController@destroy');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['cors', 'json.response']], function () {

});
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});
